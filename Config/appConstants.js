
'use strict';

var SERVER = {
    APP_NAME: 'csvUpload',
    PORTS: {
        HAPI: 8000
    },
    TOKEN_EXPIRATION_IN_MINUTES: 600,
    JWT_SECRET_KEY: '1233',
    GOOGLE_API_KEY : '',
    COUNTRY_CODE : '+91',

    THUMB_WIDTH : '40%',
    THUMB_HEIGHT : '40%',

    DOMAIN_NAME : 'http://localhost:8000/',
    SUPPORT_EMAIL : ''
};


var DATABASE = {

    USER_ROLES: {
        ADMIN: 'ADMIN',
        USER: 'USER',
        BUYER:'BUYER',
        SELLER:'SELLER'
    },

    PROFILE_PIC_PREFIX : {
        ORIGINAL : 'profilePic_',
        THUMB : 'profileThumb_',
        THUMB2: 'profileThumb2_'
    },

    DOCUMENT_PREFIX : 'document_',

    FILE_TYPES: {
        LOGO: 'LOGO',
        DOCUMENT: 'DOCUMENT',
        OTHERS: 'OTHERS'
    },
};

var STATUS_MSG = {
    ERROR: {

        ERROR: {
            statusCode:400,
            customMessage : 'Implementation Error',
            type : 'IMPLEMENTATION_ERROR'
        },
    },
    SUCCESS: {

        DEFAULT: {
            statusCode:200,
            customMessage : 'Success',
            type : 'DEFAULT'
        },
        UPDATED: {
            statusCode:200,
            customMessage : 'Updated Successfully',
            type : 'UPDATED'
        },
        LOGOUT: {
            statusCode:200,
            customMessage : 'Logged Out Successfully',
            type : 'LOGOUT'
        },
    }
};


var swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];


var APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    STATUS_MSG: STATUS_MSG,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages
};

module.exports = APP_CONSTANTS;
