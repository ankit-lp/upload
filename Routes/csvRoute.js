'use strict';
var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');


var csvRoute = [
    {
        method: 'POST',
        path: '/user/uploadCSV',
        config: {
            description: 'upload csv',
            tags: ['api', 'user'],

            handler: function (request, reply) {
                var payload=request.payload;

                Controller.csvController.uploadCSV(payload, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err))
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                })
            },
            payload: {
                maxBytes: 20000000,
                parse: true,
                output: 'file'
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,

                payload: {
                    csv: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('csv file'),
                }
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/user/getCsv',
        config: {
            description: 'Get Data From Db',
            tags: ['api', 'user'],

            handler: function (request, reply) {
                Controller.csvController.getDataFromDb(function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err))
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                })
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,

                payload: {}
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/user/updateDb',
        config: {
            description: 'Update Data From Db',
            tags: ['api', 'user'],

            handler: function (request, reply) {
                var payload=request.payload;

                Controller.csvController.updateDb(payload,function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err))
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                })
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,

                payload: {
                    dataId:Joi.string().required(),
                    name:Joi.string().optional(),
                    country:Joi.string().optional(),
                    product:Joi.string().optional(),
                    price:Joi.number().optional(),

                }
            },
            plugins: {
                'hapi-swagger': {
                    payloadType:"form-data",
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/user/listData',
        config: {
            description: 'List Data From Db',
            tags: ['api', 'user'],

            handler: function (request, reply) {
                var payload=request.payload;
                Controller.csvController.listData(payload,function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err))
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                })
            },
            validate: {
                failAction: UniversalFunctions.failActionFunction,

                payload: {
                    skip:Joi.number().required(),
                    limit:Joi.number().required(),

                }
            },
            plugins: {
                'hapi-swagger': {
                    payloadType:"form-data",
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
];



module.exports = csvRoute;
