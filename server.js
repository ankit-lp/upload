'use strict';
var Hapi = require('hapi');
var Routes = require('./Routes');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Path=require('path')
var mongoose = require('mongoose');
var Plugins = require('./Plugins');
mongoose.Promise = global.Promise;


var server = new Hapi.Server();

server.connection(
    { port:8000,
    routes: { cors: true },
});

server.register(Inert, function () {
    server.route( {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: { path: Path.normalize(__dirname + '/') }
        }
    });
})

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/upload');

const options = {
    info: {
        'title': 'UPLOAD API Documentation',
        'version': Pack.version
    }
};

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], function (err) {
    if (err){
        server.error('Error while loading plugins : ' + err)
    }else {
        server.log('info','Plugins Loaded')
    }
});

server.register(Plugins, function (err) {
    if (err){
        server.error('Error while loading plugins : ' + err)
    }else {
        server.log('info','Plugins Loaded')
    }
});

server.register(Inert, function(err){
    if(err){
        throw err;
    }
    server.route(Routes);
});



server.start(
    console.log('Server running at:', server.info.uri)
);

