var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var csv = new Schema({
    name:{type: String,default:"",trim:true},
    country:{type: String,default:"",trim:true},
    product:{type: String,default:"",trim:true},
    price:{type: Number,default:0,trim:true},
});


module.exports = mongoose.model('csv', csv);