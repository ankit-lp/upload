"use strict";
var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var fsExtra = require('fs-extra');
var Fs = require('fs');
const Thumbler = require('thumbler');

var awsUpload= function (fname,data,callback){
    console.log("bnzc.....",fname)
    var imgLink;
    AWS.config.update({
        accessKeyId: Config.awsS3Config.s3BucketCredentials.accessKeyId,
        secretAccessKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
        region:'us-west-2'
    });
    var s3 = new AWS.S3();
    var cType=fname.split('.');
    let keyname=new Date().getTime()+fname;
    async.auto({
        uploadImage:function (cb) {
            var params = {
                Bucket: Config.awsS3Config.s3BucketCredentials.bucket,
                Key: keyname,
                Body: data,
                ACL: 'public-read',
                ContentType: cType[(cType.length)-1],
            };
            s3.putObject(params, function (err, res) {
                if (err) {
                    cb(err)
                } else {
                    imgLink="https://ticketmgmt.s3-us-west-2.amazonaws.com/"+keyname;
                    cb(null)
                }
            });
        },
    },function (err,result) {
        if(err){
            callback(err)
        }else{
            callback(null,imgLink)
        }
    })
};




module.exports = {
    awsUpload: awsUpload,
};
