'use strict';

const Service = require('../Services');
const async = require('async');
const fs = require('fs');
const fsExtra = require('fs-extra');
const mongoXlsx = require('mongo-xlsx');


let uploadCSV=function (fileName,callback) {
    var path='/home/ankit/Downloads/'+fileName.csv.filename;
    async.auto({

        storeBuffer:function (cb) {
            fsExtra.copy(path,'./uploads/'+"SalesJan2009.csv", function (err,result) {
                if (err){
                    cb(err)
                }else{
                    cb(null)
                }
            });
        },

        readFile:['storeBuffer',function (err, cb) {
            var readPath='./uploads/'+"SalesJan2009.csv";
            var readStream = fs.createReadStream(readPath);
            readStream.on('data', function(data) {
                readStream.pause();

                var array=[];
                array=data.toString().split("\n");
                var dataToPush={};

                array.forEach(function (obj1) {
                    var array1=[];
                    array1=obj1.split(",");
                    dataToPush.product=array1[0];
                    dataToPush.price=array1[1];
                    dataToPush.name=array1[2];
                    dataToPush.country=array1[3];

                    Service.csvServices.createCsv(dataToPush, function (err, result) {
                        dataToPush={};
                    })
                })
                readStream.resume();
            });
            readStream.on('end', function() {});
            cb(null)
        }]

    },function (err, result) {
        callback(err,result)
    })
};

let getDataFromDb = function (callback) {
    let response;
    let path="";
    async.auto({

        getData: function (cb) {
            var criteria = {};

            Service.csvServices.getCsv(criteria, {_id:0,__v:0}, {lean: true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result && result.length){
                        response = result;
                        cb(null);
                    }else{
                        callback('No Data to show')
                    }
                }
            })
        },

        putData: ['getData', function (err,cb) {
            var model = mongoXlsx.buildDynamicModel(response);
            mongoXlsx.mongoData2Xlsx(response,model,function(err, data) {
                if(err){
                    cb(err)
                }else{
                    path=data.fullPath;
                    cb(null)
                }
            });

        }],

        /////////////UPLOAD THIS FILE TO BUCKET
    },function (err, result) {
        callback(err,path)
    })
};

let listData = function (payloadData, callback) {
    let response;
    async.auto({
        getData: function (cb) {
            var criteria = {};
            var projection={__v:0};
            var option={
                lean: true,
                skip:payloadData.skip,
                limit:payloadData.limit
            }

            Service.csvServices.getCsv(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result && result.length){
                        response = result;
                        cb(null);
                    }else{
                        callback('No Data to show')
                    }
                }
            })
        },
    },function (err, result) {
        callback(err,response)
    })
};

let updateDb = function (payloadData, callback) {
    let response;
    async.auto({
        getAndUpdateData: function (cb) {
            var criteria = {
                _id:payloadData.dataId
            };
            var projection={};
            if(payloadData.name){
                projection.name=payloadData.name
            }
            if(payloadData.country){
                projection.country=payloadData.country
            }
            if(payloadData.product){
                projection.product=payloadData.product
            }
            if(payloadData.price){
                projection.price=payloadData.price
            }
            var option={new:true};

            Service.csvServices.updateCsv(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    response = result;
                    cb(null);
                }
            })
        },
    },function (err, result) {
        callback(err,response)
    })
};


module.exports = {
    uploadCSV: uploadCSV,
    getDataFromDb:getDataFromDb,
    updateDb:updateDb,
    listData:listData
};