'use strict';

var Models = require('../Models');

var getCsv = function (criteria, projection, options, callback) {
    Models.csv.find(criteria, projection, options, callback);
};

var createCsv = function (objToSave, callback) {
    new Models.csv(objToSave).save(callback)
};

var updateCsv = function (criteria, dataToSet, options, callback) {
    Models.csv.findOneAndUpdate(criteria, dataToSet, options, callback);
};


module.exports = {
    getCsv: getCsv,
    createCsv: createCsv,
    updateCsv: updateCsv,
};

